'''
Write a function to find the longest common prefix string amongst an array of strings.

If there is no common prefix, return an empty string "".

 

Example 1:

Input: strs = ["flower","flow","flight"]
Output: "fl"
Example 2:

Input: strs = ["dog","racecar","car"]
Output: ""
Explanation: There is no common prefix among the input strings.
 

Constraints:

1 <= strs.length <= 200
0 <= strs[i].length <= 200
strs[i] consists of only lower-case English letters.
'''

def longestCommonPrefix(strs) -> str:
    lcp=''
    if not strs:
        return lcp
    
    i=0
    while True:
        l=None
        for word in strs:
            w_size=len(word)
            if l==None and i<w_size:
                l=word[i]
            elif i>=w_size or l!=word[i]:
                return lcp
            
        lcp+=l
        i+=1
    
    return lcp