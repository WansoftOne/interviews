def isPalindrome(s: str) -> bool:
    s = s.lower()
    start=0
    end=len(s)-1
    
    while start<end:
        if (not s[start].isalpha()) and (not s[start].isnumeric()):
            start+=1
        elif (not s[end].isalpha()) and (not s[end].isnumeric()):
            end-=1
        elif s[start] != s[end]:
            return False
        else:
            start+=1
            end-=1
    return True

if __name__ == '__main__':
    s1='A man, a plan, a canal: Panama'
    s2='race a car'

    #it should be true
    isPalindrome(s1)
    #it should be false
    isPalindrome(s2)